const GURPS4E = {}

CONFIG.ChatMessage.template = "systems/gurps4e/templates/chat/chat-message.html"

CONFIG.postures = [
    "systems/gurps4e/icons/postures/standing.png",
    "systems/gurps4e/icons/postures/sitting.png",
    "systems/gurps4e/icons/postures/crouching.png",
    "systems/gurps4e/icons/postures/crawling.png",
    "systems/gurps4e/icons/postures/kneeling.png",
    "systems/gurps4e/icons/postures/lyingback.png",
    "systems/gurps4e/icons/postures/lyingprone.png",
    "systems/gurps4e/icons/postures/sittingchair.png"
]

CONFIG.sizemods = [
    "systems/gurps4e/icons/sizemods/smneg1.png",
    "systems/gurps4e/icons/sizemods/smneg2.png",
    "systems/gurps4e/icons/sizemods/smneg3.png",
    "systems/gurps4e/icons/sizemods/smneg4.png",
    "systems/gurps4e/icons/sizemods/smpos1.png",
    "systems/gurps4e/icons/sizemods/smpos2.png",
    "systems/gurps4e/icons/sizemods/smpos3.png",
    "systems/gurps4e/icons/sizemods/smpos4.png"
]

CONFIG.crippled = [
    "systems/gurps4e/icons/crippled/crippledleftarm.png",
    "systems/gurps4e/icons/crippled/crippledlefthand.png",
    "systems/gurps4e/icons/crippled/crippledleftleg.png",
    "systems/gurps4e/icons/crippled/crippledleftfoot.png",
    "systems/gurps4e/icons/crippled/crippledrightarm.png",
    "systems/gurps4e/icons/crippled/crippledrighthand.png",
    "systems/gurps4e/icons/crippled/crippledrightleg.png",
    "systems/gurps4e/icons/crippled/crippledrightfoot.png",
]
CONFIG.statusEffects = [
    "systems/gurps4e/icons/postures/standing.png",
    "systems/gurps4e/icons/postures/sitting.png",
    "systems/gurps4e/icons/postures/crouching.png",
    "systems/gurps4e/icons/postures/crawling.png",
    "systems/gurps4e/icons/postures/kneeling.png",
    "systems/gurps4e/icons/postures/lyingback.png",
    "systems/gurps4e/icons/postures/lyingprone.png",
    "systems/gurps4e/icons/postures/sittingchair.png",
    "systems/gurps4e/icons/conditions/shock1.png",
    "systems/gurps4e/icons/conditions/shock2.png",
    "systems/gurps4e/icons/conditions/shock3.png",
    "systems/gurps4e/icons/conditions/shock4.png",
    "systems/gurps4e/icons/conditions/reeling.png",
    "systems/gurps4e/icons/conditions/tired.png",
    "systems/gurps4e/icons/conditions/collapse.png",
    "systems/gurps4e/icons/conditions/unconscious.png",
    "systems/gurps4e/icons/conditions/minus1xhp.png",
    "systems/gurps4e/icons/conditions/minus2xhp.png",
    "systems/gurps4e/icons/conditions/minus3xhp.png",
    "systems/gurps4e/icons/conditions/minus4xhp.png",
    "systems/gurps4e/icons/conditions/stunned.png",
    "systems/gurps4e/icons/conditions/surprised.png",
    "systems/gurps4e/icons/defeated.png",
    "systems/gurps4e/icons/blank.png",
    "systems/gurps4e/icons/crippled/crippledleftarm.png",
    "systems/gurps4e/icons/crippled/crippledlefthand.png",
    "systems/gurps4e/icons/crippled/crippledleftleg.png",
    "systems/gurps4e/icons/crippled/crippledleftfoot.png",
    "systems/gurps4e/icons/crippled/crippledrightarm.png",
    "systems/gurps4e/icons/crippled/crippledrighthand.png",
    "systems/gurps4e/icons/crippled/crippledrightleg.png",
    "systems/gurps4e/icons/crippled/crippledrightfoot.png",
    "systems/gurps4e/icons/sizemods/smneg1.png",
    "systems/gurps4e/icons/sizemods/smneg2.png",
    "systems/gurps4e/icons/sizemods/smneg3.png",
    "systems/gurps4e/icons/sizemods/smneg4.png",
    "systems/gurps4e/icons/sizemods/smpos1.png",
    "systems/gurps4e/icons/sizemods/smpos2.png",
    "systems/gurps4e/icons/sizemods/smpos3.png",
    "systems/gurps4e/icons/sizemods/smpos4.png",
]

CONFIG.controlIcons.defeated = "systems/gurps4e/icons/defeated.png";

CONFIG.JournalEntry.noteIcons = {
    "Marker": "systems/gurps4e/icons/buildings/point_of_interest.png",
    "Apothecary": "systems/gurps4e/icons/buildings/apothecary.png",
    "Beastmen Herd 1": "systems/gurps4e/icons/buildings/beastmen_camp1.png",
    "Beastmen Herd 2": "systems/gurps4e/icons/buildings/beastmen_camp2.png",
    "Blacksmith": "systems/gurps4e/icons/buildings/blacksmith.png",
    "Bretonnian City 1": "systems/gurps4e/icons/buildings/bret_city1.png",
    "Bretonnian City 2": "systems/gurps4e/icons/buildings/bret_city2.png",
    "Bretonnian City 3": "systems/gurps4e/icons/buildings/bret_city3.png",
    "Bretonnian Worship": "systems/gurps4e/icons/buildings/bretonnia_worship.png",
    "Caste Hill 1": "systems/gurps4e/icons/buildings/castle_hill1.png",
    "Caste Hill 2": "systems/gurps4e/icons/buildings/castle_hill2.png",
    "Caste Hill 3": "systems/gurps4e/icons/buildings/castle_hill3.png",
    "Castle Wall": "systems/gurps4e/icons/buildings/castle_wall.png",
    "Cave 1": "systems/gurps4e/icons/buildings/cave1.png",
    "Cave 2": "systems/gurps4e/icons/buildings/cave2.png",
    "Cave 3": "systems/gurps4e/icons/buildings/cave3.png",
    "Cemetery": "systems/gurps4e/icons/buildings/cemetery.png",
    "Chaos Portal": "systems/gurps4e/icons/buildings/chaos_portal.png",
    "Chaos Worship": "systems/gurps4e/icons/buildings/chaos_worship.png",
    "Court": "systems/gurps4e/icons/buildings/court.png",
    "Dwarf Beer": "systems/gurps4e/icons/buildings/dwarf_beer.png",
    "Dwarf Hold 1": "systems/gurps4e/icons/buildings/dwarf_hold1.png",
    "Dwarf Hold 2": "systems/gurps4e/icons/buildings/dwarf_hold2.png",
    "Dwarf Hold 3": "systems/gurps4e/icons/buildings/dwarf_hold3.png",
    "Empire Barracks": "systems/gurps4e/icons/buildings/empire_barracks.png",
    "Empire City 1": "systems/gurps4e/icons/buildings/empire_city1.png",
    "Empire City 2": "systems/gurps4e/icons/buildings/empire_city2.png",
    "Empire City 3": "systems/gurps4e/icons/buildings/empire_city3.png",
    "Farm": "systems/gurps4e/icons/buildings/farms.png",
    "Food": "systems/gurps4e/icons/buildings/food.png",
    "Guard Post": "systems/gurps4e/icons/buildings/guards.png",
    "Haunted Hill": "systems/gurps4e/icons/buildings/haunted_hill.png",
    "Haunted Wood": "systems/gurps4e/icons/buildings/haunted_wood.png",
    "Inn 1": "systems/gurps4e/icons/buildings/inn1.png",
    "Inn 2": "systems/gurps4e/icons/buildings/inn2.png",
    "Kislev City 1": "systems/gurps4e/icons/buildings/kislev_city1.png",
    "Kislev City 2": "systems/gurps4e/icons/buildings/kislev_city2.png",
    "Kislev City 3": "systems/gurps4e/icons/buildings/kislev_city3.png",
    "Lumber": "systems/gurps4e/icons/buildings/lumber.png",
    "Magic": "systems/gurps4e/icons/buildings/magic.png",
    "Metal": "systems/gurps4e/icons/buildings/metal.png",
    "Mountain 1": "systems/gurps4e/icons/buildings/mountains1.png",
    "Mountain 2": "systems/gurps4e/icons/buildings/mountains2.png",
    "Orcs": "systems/gurps4e/icons/buildings/orcs.png",
    "Orc Camp": "systems/gurps4e/icons/buildings/orc_city.png",
    "Port": "systems/gurps4e/icons/buildings/port.png",
    "Road": "systems/gurps4e/icons/buildings/roads.png",
    "Ruins": "systems/gurps4e/icons/buildings/ruins.png",
    "Scroll": "systems/gurps4e/icons/buildings/scroll.png",
    "Sigmar": "systems/gurps4e/icons/buildings/sigmar_worship.png",
    "Stables": "systems/gurps4e/icons/buildings/stables.png",
    "Standing Stones": "systems/gurps4e/icons/buildings/standing_stones.png",
    "Swamp": "systems/gurps4e/icons/buildings/swamp.png",
    "Temple": "systems/gurps4e/icons/buildings/temple.png",
    "Textile": "systems/gurps4e/icons/buildings/textile.png",
    "Tower 1": "systems/gurps4e/icons/buildings/tower1.png",
    "Tower 2": "systems/gurps4e/icons/buildings/tower2.png",
    "Tower Hill": "systems/gurps4e/icons/buildings/tower_hill.png",
    "Wizard Tower": "systems/gurps4e/icons/buildings/wizard_tower.png",
    "Ulric": "systems/gurps4e/icons/buildings/ulric_worship.png",
    "Village 1": "systems/gurps4e/icons/buildings/village1.png",
    "Village 2": "systems/gurps4e/icons/buildings/village2.png",
    "Village 3": "systems/gurps4e/icons/buildings/village3.png",
    "Wood Elves 1": "systems/gurps4e/icons/buildings/welves1.png",
    "Wood Elves 2": "systems/gurps4e/icons/buildings/welves2.png",
    "Wood Elves 3": "systems/gurps4e/icons/buildings/welves3.png"
}

GURPS4E.locations = {
    "head": "Head",
    "skull": "Skull",
    "face": "Face",
    "neck": "Neck",
    "torso": "Torso",
    "chest": "Chest",
    "abdomen": "Abdomen",
    "groin": "Groin",
    "arms": "Arms",
    "shoulders": "Shoulders",
    "upperarms": "Upper Arms",
    "elbows": "Elbows",
    "forearms": "Forearms",
    "hands": "Hands",
    "legs": "Legs",
    "thighs": "Thighs",
    "knees": "Knees",
    "shins": "Shins",
    "feet": "Feet"
}

GURPS4E.defences = {
    "dodge": "Dodge",
    "parry": "Parry",
    "block": "Block"
}

GURPS4E.traitCategories = {
    "adv": "Advantage",
    "disadv": "Disadvantage",
    "perk": "Perk",
    "quirk": "Quirk"
}

GURPS4E.rollableCategories = {
    "skill": "Skill",
    "tech": "Technique",
    "spell": "Spell",
    "rms": "Ritual Magic Spell"
}

GURPS4E.tokenSizes = {
    "tiny": 0.3,
    "ltl": 0.5,
    "sml": 0.8,
    "avg": 1,
    "lrg": 2,
    "enor": 3,
    "mnst": 4
}

GURPS4E.conditions = {
	"reeling": "GURPS4E.ConditionName.Reeling",
	"collapsehp": "GURPS4E.ConditionName.CollapseHP",
	"minusxhp": "GURPS4E.ConditionName.MinusxHP",
	"tired": "GURPS4E.ConditionName.Tired",
	"collapsefp": "GURPS4E.ConditionName.CollapseFP",
	"stunned": "GURPS4E.ConditionName.Stunned",
	"surprised": "GURPS4E.ConditionName.Surprised",
	"unconscious": "GURPS4E.ConditionName.Unconscious",
	"defeated": "GURPS4E.ConditionName.Defeated"
}


GURPS4E.conditionDescriptions = {
	"reeling": "GURPS4E.Conditions.Reeling",
	"collapsehp": "GURPS4E.Conditions.CollapseHP",
	"minusxhp": "GURPS4E.Conditions.MinusxHP",
	"tired": "GURPS4E.Conditions.Tired",
	"collapsefp": "GURPS4E.Conditions.CollapseFP",
	"stunned": "GURPS4E.Conditions.Stunned",
	"surprised": "GURPS4E.Conditions.Surprised",
	"unconscious": "GURPS4E.Conditions.Unconscious",
	"defeated": "GURPS4E.Conditions.Defeated"
}
